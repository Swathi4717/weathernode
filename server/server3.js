/**
 * A more friendly server that responds based on request URL
 */
const http = require("http")
const port = 8083

const server = http.createServer((req, res) => {
  const { headers, method, url } = req
  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.write("<!doctype html>")
  res.write("<html><head><title>Server3</title></head>")
  res.write("<body>")
  res.write("<h1>Hello Client!</h1>")
  res.write("<h1>You made a GET request from the URL " + url + "<h1>")
  res.write("<p>We can use the URL to figure out what the client wants, and route requests accordingly<p>")
  res.end("</body></html>")
})

server.listen(port, (err) => {
  if (err) { return console.log('Error', err) }
  console.log(`Server listening at http://127.0.0.1:${port}`)
})

// How do we call this? How do we make a request?
// How do we see our app?  What will appear? 
// Does this ever change?
